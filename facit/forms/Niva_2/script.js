//knappar för att hantera popup
const btnPopup = document.querySelector(".btn-popup");
const closeBtns = document.querySelectorAll(".close");

//element i login-formuläret
const login = document.querySelector(".login");
const regLink = document.getElementById("reg-link");

//element i regg-formuläret
const regg = document.querySelector(".regg");
const reggBtn = document.getElementById("regg-btn");
const reggEmail = document.getElementById("regg-email");
const reggPass = document.getElementById("regg-password");

btnPopup.addEventListener("click", () => {
  // ändra class hidden på login-div
  login.classList.remove("hidden");
});

regLink.addEventListener("click", () => {
  login.classList.add("hidden");
  regg.classList.remove("hidden");
});

reggBtn.addEventListener("click", (e) => {
  e.preventDefault()
  // ta hand om värdet som bor i inputfälten
  const newEmail = reggEmail.value;
  const newPass = reggPass.value;

 document.getElementById("email").value = newEmail;
 document.getElementById("password").value = newPass;


  login.classList.remove("hidden");
  regg.classList.add("hidden");

  reggEmail.value = "";
  reggPass.value = "";
  
});

closeBtns.forEach((button) => {
  button.addEventListener("click", () => {
    login.classList.add("hidden");
    regg.classList.add("hidden");
  });
});
