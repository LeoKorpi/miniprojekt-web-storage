/***
 * !! Demodag !!
 * Lovisa: saveNote(), clearAllNotes(), showOneNote(), editNote(), deleteNote()
 * William: start(), alla knappar, showAllNotes(), generateUniqueId()
 */

const buttons = document.querySelectorAll(".button");
const textArea = document.querySelector("#note-text");
const noteList = document.querySelector("#note-list");
const noteContent = document.querySelector("#note-content");

buttons.forEach((button) => {
  button.addEventListener("click", () => {
    handleButtonClick(button);
  });
});

function handleButtonClick(button) {
  switch (button.id) {
    case "save-note-btn":
      saveNote();
      break;
    case "clear-notes-btn":
      cleaAllNotes();
      break;
    case "edit-note-btn":
      editNote();
      break;
    case "delete-note-btn":
      deleteNote();
      break;
  }
}

function showAllNotes() {
  for (let i = 0; i < localStorage.length; i++) {
    const key = localStorage.key(i);
    const localData = localStorage.getItem(key);
    const li = document.createElement("li");
    const a = document.createElement("a");
    li.classList = "note-item";
    noteList.appendChild(li);
    li.appendChild(a);
    a.innerText = localData;
    a.href = "note.html?id=" + key;
  }
}

function generateUniqueId() {
  return "id-" + Date.now() + "-" + Math.random().toString(36).substring(2, 9);
}

function showOneNote() {
  // Hämtar URL:en som en sträng-variabel och hämtar sedan
  // värdet som matchar med strängen från local storage
  const noteId = new URLSearchParams(window.location.search).get("id");
  noteContent.innerText = localStorage.getItem(noteId);
}

function saveNote() {
  if (textArea.value < 1) {
    alert("Error! Anteckningen måste innehålla minst ett tecken");
  } else {
    //Tar värdet som skrivits in i textrutan, genererar unikt id
    let note = textArea.value;
    const randomId = generateUniqueId();
    //Sparar anteckningen i localStorage med id som nyckelvärde
    localStorage.setItem(randomId, note);
    location.href = "index.html";
  }
}

function cleaAllNotes() {
  //bekräftelse - ok/avbryt
  const confirmDeleteAll = confirm("Vill du ta bort alla anteckningar?");
  if (confirmDeleteAll) {
    //färdig funktion som rensar allt i localStorage
    localStorage.clear(); 
    //laddar om sidan på nytt
    location.href = "index.html"; 
  }
}

function editNote() {
  const noteId = new URLSearchParams(window.location.search).get("id");
  //prompt som visar anteckningens befintliga värde
  let editedMessage = prompt(
    "Redigera anteckning:",
    localStorage.getItem(noteId)
  );

  if (editedMessage !== "") {
    //byter anteckningens värde med det som skrevs in i prompten
    localStorage.setItem(noteId, editedMessage);
    showOneNote();
  } else {
    alert("Anteckningen måste innehålla minst ett tecken");
  }
}

function deleteNote() {
  const noteId = new URLSearchParams(window.location.search).get("id");
  //bekräftelse - ok/avbryt
  const confirmDelete = confirm("Vill du ta bort denna anteckning?");
  if (confirmDelete) {
    //färdig funktion som tar bort anteckningen med det id från localStorage
    localStorage.removeItem(noteId);
    window.location.href = "index.html";
  }
}

function start() {
  if (location.pathname.includes("note")) {
    showOneNote();
  } else if (location.pathname.includes("index")) {
    showAllNotes();
  } else if (location.pathname.includes("new")) {
    textArea.focus();
  }
}

start();
